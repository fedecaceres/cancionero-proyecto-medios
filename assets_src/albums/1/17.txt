.A9             Bm/A
_He perdido la luz
.A9           Bm/A
_dónde se escondió
.A9            Bm/A
_alguien se la llevó
.A9            Bm/A
_o quizás escapó.

_Nada puedo ver
_en la oscuridad
_algo debo hacer
_con mi soledad.

.D9/A            E/A
_Siento una voz en mi corazón
.D9/A                E/A
_que me invita a caminar
.Fmaj7/A                    G6/A
_y en el cielo oscuro una estrella
.Fmaj7/A          G6/A
_me ofrece su claridad.

_Monto en mi sereno corcel
_y alisto mi corazón
_la estrella me indica el camino
_y avanzo con decisión.

.A9                 B/A Bm/A A9
/Luz que me das compañía
.A9                 B/A Bm/A A9
/fuente de toda esperanza
.A9               B/A Bm/A A9
/me inspira tu melodía
.A9         B/A    Bm/A              A9
/cuando estás viva dentro de mi alma.

_Oigo a veces un sutil rumor
_que me invita a detenerme aquí
_es entonces cuando debo yo
_mantenerme firme hasta el fin.

_Siento una voz en mi corazón
_que me invita a caminar
_y en el cielo oscuro una estrella
_me ofrece su claridad.

_Monto en mi sereno corcel
_y alisto mi corazón
_la estrella me indica el camino
_y avanzo con decisión.

/Luz que me das compañía
/fuente de toda esperanza
/me inspira tu melodía
/cuando estás viva dentro de mi alma

.D9/A             E/A
_Ya percibo un fulgor matinal
.D9/A               E/A
_promesa de un nuevo sol
.D9/A             E/A
_otros jinetes explorando están
.D9/A
_quizás pueda yo

.              A9
_compartir la luz.