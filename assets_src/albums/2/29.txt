.   Am                    Fmaj7/A
_Yo soy el sol de la sabiduría
.  Am            Fmaj7/A
_y el océano del conocimiento
. Am                Fmaj7/A
_animo a los desfallecidos
.  Am                 Fmaj7/A
_y hago revivir a los muertos
.   Am                 Fmaj7/A            Am Fmaj7/A
_Yo soy la luz de guía que ilumina el camino

.C                  Em
/Yo soy el Halcón Real
.               F                C
/posado en el brazo del Todopoderoso.
.C                   Em
/Despliego las alas caídas
.        F
/de toda ave herida
.     Fm                  Am
/y le ayudo a remontar el vuelo.

.  Fmaj7/A
_Yo soy
.  Am
_Yo soy
.  Fmaj7/A
_Yo soy
.  Am
_Yo soy