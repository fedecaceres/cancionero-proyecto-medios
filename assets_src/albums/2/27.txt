.Dm Am Dm
_Intro

_¡Tun tun!
_Suena la puerta y es mi mamá, sí
_Es hora de levantarse
_Ella ya preparó el desayuno y se va a trabajar
_Eso me motiva a mí a ayudar
_a mi hermano voy a despertar
_Pensé en tirarle agua
_Pero no creo que le vaya a gustar
_Luego de eso reflexioné
_Que debo ser amable con él
_Que nuestro papá se fue
_antes de que él lo pudiera conocer
_Todos somos hermanos, fuimos creados para amar
_Debemos tejer con los hilos que nos dan
_Cada decisión es una oportunidad
_Para ir bordando un patrón espiritual

/Con lo que nos dan debemos bordar
/En nuestro patrón espiritual

_Rumbo al colegio
_Con mi hermano al lado
_Vamos en el bus un poquito apretados
_Como 30 personas, todas sin igual
_Con ojos de fe puedo ver su potencial
_Poco a poco más gente se está sumando
_Siete mil millones, este bus se está llenando
_De ricos, pobres, altos, flacos, gordos
_Chicas, chicos, chatos, todos a bordo
_Verde, amarillo, morado y rosado
_Ojos celestes, cafés y anaranjados
_Todos avanzando por esta calle real
_¡Chofer! Ponte una canción celestial

.Dm           Em            Bb
/Quiero aprender, voy a ayudar
.          Am                  Dm
/Quiero sentir el ritmo celestial
.         Em             Bb
/En el servicio a los demás
.         Am                  Dm
/Quiero sentir el ritmo celestial

_En la clase de lengua el profe habla al infinito
_Los estudiantes viajan por el espacio
_Supuestamente estamos en un mismo salón
_Pero dime, ¡quién está prestando atención!

_Así ruge el león del lenguaje
_Ya comprendo por qué siente coraje
_Para entender tenemos que escuchar
_Cada quien tiene algo que aportar
_Para que las palabras no se las lleve el viento
_Hay que pensar en lo que llevan por dentro
_La expresión es la chispa que enciende
_El fuego del conocimiento

_Después de clases converso con mi hermano
_Nos divertimos y la casa arreglamos
_Esperamos a que llegue mamá
_Y juntos ponernos a cocinar
_Como todas las semanas nuestra casa ya está lista
_Mi hermano está en la puerta recibiendo a las
_visitas
_Son nuestros vecinos, nos reunimos para orar
_Pues nuestra relación se mueve a un ritmo
_espiritual

/Quiero aprender, voy ayudar
/Quiero sentir el ritmo celestial
/En el servicio a los demás
/Quiero sentir el ritmo celestial