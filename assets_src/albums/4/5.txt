_Recitado:
/Dicen que cada persona
/trabaja sólo para su propio bien
/y que a otros debe superar.
/Dicen que en mí no pueden confiar.
/Que muy poco puedo hacer por los demás.
/¡Pero yo digo que no!

.Am             F           C
_Parece que la lucha por ganar
.             E7              Am
_es siempre a costa de los demás,
.           F                  C
_cada uno piensa en su bienestar
.E7           Am
_en su bienestar.

_Veo gente que quiere trabajar.
_Una mano amiga para avanzar.
_Hacia una nueva humanidad
.E7           Dm
_nueva humanidad.

.Dm              C            G
_Puedes ver la realidad interior,
.Dm            C           G
_gracias a la fe y a la razón.

.Am     F
/Oh oh oh
.        C         E7
/Puedes ver más allá
.               Am
/(Ver la realidad)
.Am    F
/Oh oh oh
.        C         E7
/Puedes ver más allá
.              Am
/(Con el corazón)

_Parece que el mundo no va a cambiar,
_que la gente siempre va a dudar.
_Todos tienen miedo de actuar
_miedo de actuar.

_Veo en los amigos seguridad,
_porque ya conocen su potencial.
_Tienen la confianza para avanzar
_para avanzar.

_Puedes ver la realidad interior,
_gracias a la fe y a la razón.

/Oh oh oh
/Puedes ver más allá
/(Ver la realidad)
/Oh oh oh
/Ouedes ver más allá
/(Con el corazón)

_Recitado:
/Y saldrá de la duda para hallar la certeza
/y se volverá de las oscuras ilusiones
/hacia la luz de guía…
/Se abrirá su vista interior
/y conversará intimamente con el Bienamado;
/entreabrirá el portal de la verdad y la piedad.

/Oh oh oh
/Puedes ver más allá
/(Ver la realidad)
/Oh oh oh
/Ouedes ver más allá
/(Con el corazón)