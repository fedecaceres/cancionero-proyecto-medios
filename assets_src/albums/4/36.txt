.A  C#m  F#m  E   D   E
_Intro

.A       C#m             F#m
_Sé que ahora hay muchas cosas
.D                   E
_que tú anhelas descubrir
.A     C#m               F#m
_en tu mente hay todo un mundo
.D                E
_que deseas compartir
_Pero a veces tienes dudas
_de tu verdadero ser
_necesitas las respuestas
_me ha pasado a mí también

.D                   E
_Hoy te invito a conocer
.D                E
_la belleza de tu ser

.A
/Porque veo en ti
.   C#m  F#m
/un corazón
.   E      D
/de gran valor
.       E
/escúchalo

/Porque veo en ti
/la fuerza que
/transformará
/la sociedad

_Tus talentos escondidos
_Guardan un gran potencial
_Ten paciencia en el camino
_Con esfuerzo avanzarás
_Hoy te invito a conocer
_la belleza de tu ser

/Porque veo en ti
/un corazón
/de gran valor
/escúchalo

/Porque veo en ti
/la fuerza que
/transformará
/la sociedad

_Se abren nuevos horizontes
_todo un mar por explorar
_ven conmigo y trae tus dones
_para juntos navegar