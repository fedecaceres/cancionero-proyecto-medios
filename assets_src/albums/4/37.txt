.A
_Si tú piensas
.              G
_que tu barrio puede cambiar
.G
_algo mejor vendrá

_Si tú hablas
_sobre cómo se logrará
_algo mejor vendrá
_Si tú actúas
_y ayudas a los demás
_algo mejor vendrá

.F
/Si te levantas
.G              A
/Algo mejor vendrá

.A
_Buenos pensamientos
.A
_inspiran las palabras
.    G
_que moldearán
.D
_tu forma de actuar

_Y buenas acciones
_inspiran corazones
_que cambiarán
_a nuestra sociedad

/rap:
_Tus palabras tienen más poder que la espada
_si no llevan al bien están mal utilizadas
_Las palabras tienen un poder increíble
_su impresión es claramente perceptible
_Iluminadas por buenos pensamientos
_Intensiones puras y buenos sentimientos
_Si las palabras las conviertes en acción
_Su sentido llega al fondo del corazón.

_Si tú piensas
_que tu barrio puede cambiar
_algo mejor vendrá

_Si tú hablas
_sobre cómo se logrará
_algo mejor vendrá

_Si tú actúas
_y ayudas a los demás
_algo mejor vendrá

/Si te levantas
/Algo mejor vendrá

_Buenos pensamientos
_Inspiran las palabras
_Que moldearán
_Tú forma de actuar

_Y buenas acciones
_Inspiran corazones
_Que cambiarán
_A nuestra sociedad

/rap:
_Las acciones tienen un papel importante
_acompañan lo que dices y lo hacen
_relevante
_es pensamiento hecho realidad
_influyen en la vida de la comunidad
_Tus acciones adornadas de humildad
_Afianzan lo que sabes y quien eres en
_verdad
_Inspiradas en la divinidad
_Su efecto perdura por la eternidad

_Buenos pensamientos
_Inspiran las palabras
_Que moldearán
_Tú forma de actuar

_Y buenas acciones
_Inspiran corazones
_Que cambiarán
_A nuestra sociedad

/Si te levantas
/Algo mejor vendrá