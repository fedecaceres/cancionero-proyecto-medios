# -*- coding: utf-8 -*-

import io
import codecs
import json

def convert(this_string):
    # print this_string

    print('input type:', type(this_string))

    lines = this_string.split('\n')
    current_line_number = 0
    combined_lines = []

    chord_line = ''
    lyrics_line = ''
    is_chorus = False

    # go through each line
    while current_line_number < len(lines):
        
        # find out if there's a chord line, if so, save it and move on to the next line in this iteration
        if lines[current_line_number].startswith('.'):
            chord_line = lines[current_line_number][1:]
            current_line_number += 1
        else:
            chord_line = ''

        # get lyrics and determine if we're in a chorus
        is_chorus = lines[current_line_number].startswith('/')
        lyrics_line = lines[current_line_number][1:]

        # start the combination process
        combined_line = '';
        if chord_line:
            # ensure both the lyrics and chord lines are of the same length
            longer_line_length = max(len(lyrics_line), len(chord_line))
            lyrics_line = lyrics_line.ljust(longer_line_length)
            chord_line = chord_line.ljust(longer_line_length)
            
            # print "lyrics '%s'" % lyrics_line
            # print "chords '%s'" % chord_line

            # "zip" the two lines together
            i = 0
            found_chord = False
            while i < len(lyrics_line):
                # determine if there's a chord start in this index
                if not found_chord and chord_line[i] != ' ':
                    # trim the chord line to the current index
                    # separate it into words
                    # get the first word
                    # bliss
                    combined_line += '[%s]' % chord_line[i:].split(' ')[0]
                    found_chord = True
                elif found_chord and chord_line[i] == ' ':
                    found_chord = False

                combined_line += lyrics_line[i]
                i += 1
        else:
            # print "lyrics '%s'" % lyrics_line
            combined_line = lyrics_line;

        # add chorus directive
        if is_chorus:
            combined_line = '{soc}%s{eoc}' % combined_line

        combined_lines.append(combined_line)
        # print combined_line
        # print '==========='
        
        current_line_number += 1

    # print '\n'.join(combined_lines)
    # print type('\n'.join(combined_lines))

    print(combined_lines)
    print(type('\n'.join(combined_lines)))

    return '\n'.join(combined_lines)

def get_database_json():
    with io.open('database.json', mode='r', encoding='utf-8') as database_file:
        data = json.load(database_file)
    return data

def get_song_string(albumId, songId):
    with io.open("albums/%d/%d.txt" % (albumId, songId), mode='r', encoding='utf-8') as song_file:
        song_string = song_file.read()
    return song_string

def save_database_json(new_json):
    with io.open('../assets/database.json', mode='w', encoding='utf-8') as database_file:
        database_file.write(json.dumps(new_json, ensure_ascii=False))

def __main__():
    database_json = get_database_json()
    for album in database_json['albums']:
        print('Album', album['name'], '...')
        for song in album['songs']:
            print('    Song', song['name'], '...')
            song['lyrics'] = convert(get_song_string(album['id'], song['id']))
    print('Saving...')
    save_database_json(database_json)


__main__()
# convert(my_string)