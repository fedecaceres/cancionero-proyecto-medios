import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:quiver/core.dart';


import 'database.dart';
import 'theme.dart';
import 'redux/states.dart';
import 'redux/actions.dart';

class _SongTilePlayActionViewModel {
  bool isPlaying;
  VoidCallback callback;

  _SongTilePlayActionViewModel(this.isPlaying, this.callback);

  bool operator ==(o) => o is _SongTilePlayActionViewModel && isPlaying == o.isPlaying;
  int get hashCode => isPlaying.hashCode;
}

class _SongTileFavoriteActionViewModel {
  bool isFavorite;
  VoidCallback callback;

  _SongTileFavoriteActionViewModel(this.isFavorite, this.callback);

  bool operator ==(o) => o is _SongTileFavoriteActionViewModel && isFavorite == o.isFavorite;
  int get hashCode => isFavorite.hashCode;
}

class SongTile extends ListTile {

  Song song;
  GestureTapCallback onTap;
  TextSelection highlight;

  SongTile({
    this.song,
    this.onTap,
    this.highlight
  }) : super(
    // title: new Text(song.name),
    title: titleWithHighlight(song, highlight),
    subtitle: new Row(
      children: <Widget>[
        new Icon(Icons.cloud_download, color: new Color.fromRGBO(140, 203, 178, 1.0), size: 18.0,),
        new Flexible(
          child: new Container(
            child: new Text(song.getCleanLyricsPreview(), overflow: TextOverflow.ellipsis),
            padding: const EdgeInsets.only(left: 12.0),
          ) 
        )
      ],
    ),
    leading: StoreConnector<CancioneroPlayerState, _SongTilePlayActionViewModel> (
      distinct: true,
      converter: (store) => new _SongTilePlayActionViewModel(
        store.state.currentSong.id == song.id && store.state.playbackState == PlaybackState.Playing,
        store.state.currentSong.id == song.id ?
          (store.state.playbackState == PlaybackState.Playing ?
            () => store.dispatch(PlaybackAction.Pause) :
            () => store.dispatch(PlaybackAction.Play)) :
          () {
            store.dispatch(new SelectSongAction(song));
            store.dispatch(PlaybackAction.Play);
          }
      ),
      builder: (context, vm) =>
        new IconButton(
          icon: new Icon(vm.isPlaying ? Icons.pause : Icons.play_arrow),
          onPressed: vm.callback,
        )
    ),
    trailing: StoreConnector<CancioneroPlayerState, _SongTileFavoriteActionViewModel> (
      distinct: true,
      converter: (store) {
        return  new _SongTileFavoriteActionViewModel(
        store.state.favorites.contains(song.id),
        store.state.favorites.contains(song.id) ?
          () => store.dispatch(RemoveFavoriteAction(song.id)) :
          () => store.dispatch(AddFavoriteAction(song.id))
      );},
      builder: (context, vm) =>
        new IconButton(
          icon: Icon(vm.isFavorite ? Icons.favorite : Icons.favorite_border),
          onPressed: vm.callback,
        )
    ),
    onTap: onTap,
  );

  static Widget titleWithHighlight(Song song, TextSelection h) {
    if (h != null) {
      return new RichText(
        text: new TextSpan(
          style: appTheme().textTheme.bodyMedium.copyWith(fontSize: 16.0),
          children: [
            new TextSpan(text: song.name.substring(0, h.baseOffset)),
            new TextSpan(text: song.name.substring(h.baseOffset, h.baseOffset + h.extentOffset), style: new TextStyle(fontWeight: FontWeight.w700)),
            new TextSpan(text: song.name.substring(h.baseOffset + h.extentOffset))
          ]
        )
      );
    } else {
      return new Text(
        song.name,
        style: new TextStyle(fontWeight: FontWeight.normal, fontSize: 20.0),
      );
    }
  }
}