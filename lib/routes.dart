import 'package:flutter/material.dart';

class CustomRoute<T> extends MaterialPageRoute {
  CustomRoute({WidgetBuilder builder, RouteSettings settings})
    : super(builder : builder, settings : settings);

  @override
  Widget buildTransitions(BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    if (/*settings.isInitialRoute*/ false)
      return child;
    // Fades between routes. (If you don't want any animation, 
    // just return child.)
    // return new FadeTransition(opacity: animation, child: child);
    return new SlideTransition(
      position: new Tween<Offset>(begin: Offset(1.0, 0.0),
        end: Offset.zero,
      ).animate(animation),
      child: child
    );
  }
}