import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'theme.dart';
import 'routes.dart';
import 'songlist.dart';
import 'song.dart';
import 'database.dart';
import 'player.dart';
import 'redux/states.dart';
import 'redux/middleware.dart';
import 'redux/reducers.dart';
import 'ui.dart';

class SearchScreen extends StatefulWidget {

  String searchString;

  SearchScreen();

  @override
  State<StatefulWidget> createState() => new SearchScreenState();
}

class SearchScreenState extends State {

  String searchString;
  SearchResults searchResults = _searchSongs("");

  _goToSong(Song song) {
    Navigator.of(context).push(
      new CustomRoute(
        builder: (context) {return new SongView(song);}
      )
    );
  }

  _search(searchString) {
    print(searchString);
    setState(() {
      this.searchString = searchString;
      this.searchResults = _searchSongs(searchString);
      this.searchResults.sort();
    });
    
  }

  @override
  Widget build(BuildContext context) {
    Scaffold s = new Scaffold(
      appBar: new AppBar( 
        title: new TextField(
          autofocus: true,
          decoration: new InputDecoration(
            border: InputBorder.none,
            hintText: "Buscar",
          ),
          onChanged: (searchString) => this._search(searchString),
        ),
        iconTheme: Theme.of(context).primaryIconTheme.copyWith(color: Colors.white),
        backgroundColor: Colors.green,
      ),
      body: new Column(
        children: <Widget>[
          new Expanded(
            child: new ListView.builder(
              itemCount: searchResults.getResultsCount(),
              itemBuilder: (context, i) {
                if (i < searchResults.getResultsCount()) {
                  return new SongTile(
                    song: this.searchResults.getResults()[i].song,
                    highlight: this.searchResults.getResults()[i].match,
                    onTap: () => this._goToSong(this.searchResults.getResults()[i].song),
                  );
                }
              }
            )
          )
        ],
      )
    );

    return s;
  }
}

class SearchResults {
  List<SearchResult> results = [];

  SearchResults(this.results);

  void sort() {
    this.results.sort(
      (s1, s2) => s1.song.name.compareTo(s2.song.name)
    );
  }

  int getResultsCount() {
    return this.results.length;
  }

  List<SearchResult> getResults() {
    return this.results;
  }
}

SearchResults _searchSongs(String searchString) {
  SongDatabase _db = SongDatabase.getInstance();
  return new SearchResults(_db.searchSongs(searchString));
}