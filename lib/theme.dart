import "package:flutter/material.dart";

ThemeData appTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    primaryColor: CancioneroColors.primaryColor,
    primaryTextTheme: base.primaryTextTheme.copyWith(
      titleMedium: base.primaryTextTheme.titleMedium.copyWith(
        fontWeight: FontWeight.w400,
        fontSize: 26.0,
      )
    ).apply(
      bodyColor: Colors.black,
      fontFamily: "Rubik",
    ),
    primaryIconTheme: base.primaryIconTheme.copyWith(
      color: CancioneroColors.textOnPrimaryColor
    ),

    accentColor: Colors.orange,
    textTheme: base.textTheme.copyWith(
      titleMedium: base.textTheme.titleMedium.copyWith(
        fontWeight: FontWeight.w300,
        fontSize: 22.0
      ),
    ).apply(
      fontFamily: "Rubik"
    ),
    // iconTheme: base.iconTheme.copyWith(
    //   color: CancioneroColors.textOnPrimaryColor
    // )
  );
}

class CancioneroColors {
  // static const Color primaryColor = Color.fromRGBO(140, 203, 178, 1.0);
  static const Color primaryColor = Colors.orange;
  static const Color textOnPrimaryColor = Color(0xFF3B3B3B);

  static const Color accentColor = Colors.orange;
  
}