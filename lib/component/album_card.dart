

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../colors.dart';

class AlbumCard extends StatelessWidget {
  int id;
  String text;
  Image image;
  Color color;
  void Function() onPressed;

  AlbumCard(this.id, this.text, this.image, this.color, this.onPressed);
  
  @override
  Widget build(BuildContext context) {
    return Expanded(child: Padding(
      padding: EdgeInsets.all(10.0),
      child: Material(
        elevation: 8.0,
        // semanticContainer: true,
        color: this.color,
        shape: RoundedRectangleBorder(
          // side: BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(20),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: InkWell(
            onTap: onPressed,
              child: Column(children: [
              // new TextButton(
              //   child: 
                new Hero(
                  tag: "album-cover-${this.id}",
                  child: this.image
                ),
              //   onPressed: () {_goToAlbum(4);},
              //   style: TextButton.styleFrom(padding: EdgeInsets.zero)
              // ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
                child: Text(
                  this.text,
                  style: TextStyle(
                    color: AppColors.background,
                    fontWeight: FontWeight.w500,
                    fontSize: 16.0,
                  ),
                  
                )
              )
            ])
        )
      )
    ));
  }

}
