import "dart:ui";

import 'package:cancionero_proyecto_medios/colors.dart';
import 'package:flutter/material.dart';

class NavButton extends StatelessWidget {

  String text;
  IconData icon;
  void onTap;

  NavButton(this.text, this.icon, this.onTap);


  @override
  Widget build(BuildContext context) {
    return SizedBox.fromSize(
      size: Size(96, 72),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        child: Material(
          color: AppColors.primary,
          child: InkWell(
            splashColor: AppColors.accent, 
            onTap: () {}, 
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(this.icon, size: 28.0, color: AppColors.background),
                Divider(height: 6.0), // <-- Icon
                Text(this.text, textAlign: TextAlign.center, style: TextStyle(color: AppColors.background, fontWeight: FontWeight.bold, fontSize: 14.0)), // <-- Text
              ],
            ),
          ),
        ),
      ),
    );
  }

}
  