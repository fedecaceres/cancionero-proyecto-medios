import 'package:flutter/material.dart';
import "package:diacritic/diacritic.dart";

RegExp chordRegExp = new RegExp(r'\[([A-Za-z0-9#\/]+)\]');
RegExp directiveRegExp = new RegExp(r'\{([A-Za-z0-9#]+)\}');

class Album {
  int id;
  String name;
  List<Song> songs;

  Album(this.id, this.name, this.songs);
}

class SearchResult {
  Song song;
  TextSelection match;

  SearchResult(this.song, this.match);
}

class Song {
  int id;
  String name;
  String rawLyrics;
  List<String> _chords = [];
  String _downloadUrl;
  int albumId;

  Song(this.id, this.name, this.rawLyrics, this._downloadUrl, this.albumId);

  String get downloadUrl => _downloadUrl == null ? null : _downloadUrl;

  String getCleanLyrics() {
    return this.rawLyrics.replaceAll(chordRegExp, '').replaceAll(directiveRegExp, '');
  }

  String getCleanLyricsPreview() {
    return getCleanLyrics().replaceAll('\n', ' ');
  }

  List<String> getChords() {
    // speudo lazy-loading
    if (this._chords.length == 0) {
      Set<String> chords = new Set();
      chordRegExp.allMatches(this.rawLyrics).forEach((match) {
        chords.add(match.group(1));
      });
      this._chords = chords.toList();
    }
    
    return this._chords;
  }
}

class SongDatabase {

  List<Album> albums = [];

  // singleton shenanigans
  static final SongDatabase _db = new SongDatabase();
  static SongDatabase getInstance() {
    return _db;
  }

  init(Map<String, dynamic> json) {
    var albums = json['albums'];
    albums.forEach((albumJson) {
      //new Album(id, name, songs)

      this.albums.add(
        new Album(
          albumJson['id'],
          albumJson['name'],
          albumJson['songs'].map(
            (dynamic songJson) => new Song(
              songJson['id'],
              songJson['name'],
              songJson['lyrics'],
              songJson['media']['download']['url'],
              albumJson['id']
            )
          ).toList().cast<Song>() // this is so strange
        )
      );
    });
  }

  Album getAlbum(int id) {
    return albums.firstWhere(
      (album) => album.id == id,
      orElse: () => new Album(-1, '', [])
    );
  }

  Song getSong(int id) {
    return albums
      .map((album) => album.songs)
      .expand((album) => album)
      .firstWhere((song) => song.id == id);
  }

  List<SearchResult> searchSongs(String queryString) {
    RegExp exp = new RegExp(queryString, caseSensitive: false);
    List<SearchResult> results = [];
    for (Album album in albums) {
      for (Song song in album.songs) {
        if (removeDiacritics(song.name).contains(exp)) {
          results.add(new SearchResult(
            song,
            new TextSelection(
              baseOffset: removeDiacritics(song.name).indexOf(exp),
              extentOffset: queryString.length
            )
          ));
        }
      }
    }
    return results;
  }
  // SongDatabase(json )
  // List<Song>
}