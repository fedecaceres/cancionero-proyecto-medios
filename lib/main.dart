import 'dart:convert';

import 'package:audioplayers/audioplayers.dart';
import 'package:cancionero_proyecto_medios/component/album_card.dart';
import 'package:cancionero_proyecto_medios/component/nav_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';
import 'package:redux/redux.dart';

import 'theme.dart';
import 'routes.dart';
import 'songlist.dart';
import 'database.dart';
import 'player.dart';
import 'search.dart';
import 'redux/actions.dart';
import 'redux/states.dart';
import 'redux/middleware.dart';
import 'redux/reducers.dart';
import 'colors.dart';


void main() {

  final persistor = Persistor<CancioneroPlayerState> (
    storage: FlutterStorage(key: 'cancionero_gpj', location: FlutterSaveLocation.sharedPreferences),
    serializer: JsonSerializer<CancioneroPlayerState>(CancioneroPlayerState.fromJson),
    shouldSave: (store, action) => action is AddFavoriteAction || action is RemoveFavoriteAction
  );
  
  final Store<CancioneroPlayerState> store = new Store<CancioneroPlayerState>(
    playerReducer,
    initialState: new CancioneroPlayerState.initialState(),
    middleware: [playbackMiddleware, persistor.createMiddleware()]
  );

  persistor.load();

  _initCallbacks(store);

  runApp(new MyApp(store: store));

  rootBundle
    .loadString('assets/database.json')
    .then(
      (jsonString) => SongDatabase.getInstance().init(JsonCodec().decode(jsonString))
  );
}

_initCallbacks(Store<CancioneroPlayerState> store) {
  store.state.audioPlayer.onAudioPositionChanged.listen((position) => store.dispatch(new SetPositionAction(position)));
  store.state.audioPlayer.onDurationChanged.listen((Duration d) {
    store.dispatch(new SetDurationAction(d));
  });
  store.state.audioPlayer.onPlayerStateChanged.listen((status) {
    if (status == PlayerState.PLAYING) {
      // store.dispatch(new SetDurationAction(store.state.audioPlayer.));
    } else if (status == PlayerState.COMPLETED) {
      store.dispatch(PlaybackAction.Complete);
    }
  }, onError: (msg) {
    print("Error");
    print(msg);
  });
}

class MyApp extends StatelessWidget {

  final Store<CancioneroPlayerState> store;

  MyApp({this.store});

  @override
  Widget build(BuildContext context) {
    return new StoreProvider<CancioneroPlayerState>(
      store: store,
      child: new MaterialApp(
        title: 'Cancionero Prejuvenil',
        theme: appTheme(),
        home: new Home(),
      ),
    );
  }
}

class Home extends StatefulWidget {
  @override
  createState() => new HomeState();
}

class HomeState extends State {
  @override
  Widget build(BuildContext context) {

    void goToSearch() {
      Navigator.of(context).push(
        new CustomRoute(
          builder: (context) {return new SearchScreen();}
        )
      );
    }

    FocusNode _focusNode = new FocusNode();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _focusNode.unfocus();
        Navigator.of(context).push(
          new CustomRoute(
            builder: (context) {return new SearchScreen();}
          )
        );
      }
    });

    return new Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text(
          "Cancionero Prejuvenil",
          // style: Theme.of(context).textTheme.title,
          style: Theme.of(context).textTheme.titleLarge.apply(color: AppColors.primary).copyWith(fontWeight: FontWeight.w500)
        ),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.search),
            color: AppColors.primary,
            onPressed: goToSearch,
          )
        ],
        textTheme: Theme.of(context).primaryTextTheme,
        backgroundColor: AppColors.background,
        elevation: 0.0,
      ),
      body: new Column(
        children: [
          new Expanded(child: new CustomScrollView(
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                child: new Column(
                  children: <Widget>[
                    new Expanded(
                      child: Container(
                        color: AppColors.background,
                        padding: EdgeInsets.all(20.0),
                        child: new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Row(
                            children: <Widget> [
                              AlbumCard(4, "Claridad", new Image.asset('assets/claridad_300x300.png'), Color(0xFF886a48), () {_goToAlbum(4);}),
                              AlbumCard(3, "Avanzo con Valor", new Image.asset('assets/avanzo_con_valor_300x300.jpg'), Color(0xFF1e7412), () {_goToAlbum(3);})
                            ],
                          ),
                          new Row(
                            children: <Widget> [
                              AlbumCard(2, "Sembrando Futuro", new Image.asset('assets/sembrando_futuro_300x300.png'), Color(0xFF215494), () {_goToAlbum(2);}),
                              AlbumCard(1, "Una Decisión", new Image.asset('assets/una_decision_300x300.png'), Color(0xFFe66c1e), () {_goToAlbum(1);})
                            ],
                          ),
                        ],
                      )
                      )
                    ),
                  ],
                ),
              )
            ],
            // padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
            // color: AppColors.background,
            
          )),
          new MainBottomNavigation(onTap: (s) {
            print(s + '<<<');
          }),
          new Player(),
        ]
      ),
    );
  }

  _goToAlbum(int albumId) {
    Navigator.of(context).push(
      new CustomRoute(
        builder: (context) {return new SongListView(albumId);}
      )
    );
  }
}



class MainBottomNavigation extends StatelessWidget {
  final void Function(String) onTap;

  MainBottomNavigation({this.onTap});

  List<CustomBottomNavigationBarItem> _items = [
    new CustomBottomNavigationBarItem("albums", Icons.library_music, "Albumes"),
    new CustomBottomNavigationBarItem("favorites", Icons.favorite_border, "Favoritos"),
    new CustomBottomNavigationBarItem("about", Icons.info_outline, "Acerca de..."),
  ];

  @override
  Widget build(BuildContext context) {

    // return Row(
    //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //   children:  _items.map((item) {
    //     return NavButton(item.text, item.icon, () => {});
    //   }).toList(),

    // );

    return new Ink(
      decoration: new BoxDecoration(
        color: AppColors.background,
        boxShadow: [new BoxShadow(
          color: Colors.black,
          blurRadius: 8.0,
          offset: const Offset(0.0, 5.0)
        )]
      ),
      // height: 70.0,
      width: double.infinity,
      child: new Row(
        children: _buildOptions()
      )
    );
  }


  String _selectedPage = "albums";

  List<Widget> _buildOptions() {
    return _items.map((item) {
      // Color color = _selectedPage == item.id ? CancioneroColors.textOnPrimaryColor : Colors.amber.shade100;
      Color color = _selectedPage == item.id ? AppColors.accent : AppColors.text;
      FontWeight weight = _selectedPage == item.id ? FontWeight.normal : FontWeight.normal;
      // double fontSize = _selectedPage == item.id ? 14.0 : 12.0;
      double fontSize = 12.0;
      return new Expanded(
        child: new InkWell(
          splashColor: AppColors.primary,
          highlightColor: AppColors.primary.withAlpha(100),
          onTap: () {
            onTap(item.id);
          },
          child: new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Column(
              children: <Widget>[
                new Icon(
                  item.icon,
                  size: 28.0,
                  color: color
                ),
                new Text(
                  item.text,
                  softWrap: true,
                  overflow: TextOverflow.fade,
                  style: TextStyle(color: color, fontWeight: weight, fontSize: fontSize)
                )
              ],
            ),
          ) 
        )
      );
    }).toList();
  }
}

class CustomBottomNavigationBarItem {
  String id;
  IconData icon;
  String text;

  CustomBottomNavigationBarItem(this.id, this.icon, this.text);
}