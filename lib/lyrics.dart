import 'package:flutter/material.dart';

import 'theme.dart';

const double fontSize = 16.0;
const double lineHeight = fontSize * 1.3;

class Lyrics extends StatelessWidget {

  String lyricsText;
  bool showChords = false;

  Lyrics(this.lyricsText, {this.showChords});

  @override
  Widget build(BuildContext context) {
    return new LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return new CustomPaint(
          size: new Size(constraints.biggest.width, this.calculateHeight()),
          // size: constraints.biggest,
          painter: new LyricsPainter(
            this.lyricsText,
            constraints.biggest,
            this.showChords
          ),
          // child: new Text(this.lyricsText)
        );
      }
    );
  }

  double calculateHeight() {
    double height = 0.0;
    lyricsText.split('\n').forEach((String line) {
      height += ((line.length ~/ 45) + 1) * lineHeight;
      if (line.contains('[')) height += lineHeight;
    });

    return height;
  }
}

class ChordToken {
  final String chord;
  final int position;

  ChordToken(this.chord, this.position);

  String toString() {
    return "[" + this.chord + "] @ " + this.position.toString(); 
  }
}

class LyricsPainter extends CustomPainter {

  final String lyrics;
  final Size biggestSize;
  final bool drawChords;

  final TextStyle _lyricStyle = new TextStyle(
    fontSize: fontSize,
    fontFamily: appTheme().textTheme.bodyMedium.fontFamily,
    color: Colors.black,
  );

  final TextStyle _lyricStyleChorus = new TextStyle(
    fontSize: fontSize,
    fontFamily: appTheme().textTheme.bodyMedium.fontFamily,
    fontStyle: FontStyle.italic,
    color: Colors.black,
  );
  final TextStyle _chordStyle = new TextStyle(
    fontSize: fontSize,
    fontFamily: appTheme().textTheme.bodyMedium.fontFamily,
    color: Colors.orangeAccent,
    fontWeight: FontWeight.bold,  
  );
  

  LyricsPainter(this.lyrics, this.biggestSize, this.drawChords);

  @override
  void paint(Canvas canvas, Size size) {
    print(_lyricStyle.fontFamily);
    Stopwatch stopwatch = new Stopwatch()..start();
    print('painting...' + lineHeight.toString() + ' ' + lyrics.substring(0, 10));
    drawText(canvas);
    print('doSomething() executed in ${stopwatch.elapsed}');
    stopwatch.stop();
  }

  RegExp chordRegExp = new RegExp(r'\[([A-Za-z0-9#\/]+)\]');
  RegExp directiveRegExp = new RegExp(r'\{([A-Za-z0-9#]+)\}');

  void drawText(Canvas canvas) {
    TextPainter tp;
    
    List<String> lines = lyrics.split("\n");
    
    int lineNumber = 0;
    for (String line in lines) {
      List<ChordToken> chordTokens = [];
      int startIndex = 0, endIndex;
      
      // calculate the position of all chord tokens
      // offset = the length of the previous tokens (including the '[' and ']')
      String lineWithoutDirectives = line.replaceAll(directiveRegExp, '');
      chordRegExp.allMatches(lineWithoutDirectives).forEach((match) {
        int startOffset = chordTokens.length == 0
        ? 0
        : chordTokens.map((token) => token.chord.length + 2).  reduce((a, b) => a + b);
        chordTokens.add(new ChordToken(match.group(1), match.start - startOffset));
      });

      String cleanLine = line.replaceAll(chordRegExp, '').replaceAll(directiveRegExp, '');

      while (endIndex != cleanLine.length) {
        endIndex = cleanLine.length;
        tp = null;

        while (tp == null || (tp.size.width > biggestSize.width)) {
          TextSpan ts = new TextSpan(
            text: cleanLine.substring(startIndex, endIndex),
            style: line.startsWith('{soc}') ? this._lyricStyleChorus : this._lyricStyle
          );

          tp = new TextPainter(
            text: ts,
            textDirection: TextDirection.ltr
          )
            ..layout();

          if (tp.size.width > biggestSize.width) {
            endIndex = cleanLine.substring(startIndex, endIndex).lastIndexOf(' ') + startIndex;
          }
        }

        if (chordTokens.length > 0 && this.drawChords) {
          for (ChordToken token in chordTokens) {
            // print(token);
            if (token.position >= startIndex && token.position < endIndex) {
              List<TextBox> tb = tp.getBoxesForSelection(new TextSelection(baseOffset: token.position - startIndex, extentOffset: token.position - startIndex + 1));

              new TextPainter(
                text: new TextSpan(
                  text: token.chord,
                  style: this._chordStyle
                ),
                textDirection: TextDirection.ltr
              )
                ..layout()
                ..paint(canvas, new Offset(tb.first.left, lineNumber * lineHeight));
            }
          }
          lineNumber++;
        }

        tp.paint(canvas, new Offset(0.0, lineNumber * lineHeight));

        startIndex = endIndex + 1;
        lineNumber++;
      }
      
    }
  }

  @override
  bool shouldRepaint(LyricsPainter old) {
    print("should repoaint?");
    print(old.lyrics != lyrics);
    return old.lyrics != lyrics;
  }

}