import 'dart:async';
import 'dart:math' show max;

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:audioplayers/audioplayers.dart';

import 'database.dart';
import 'redux/actions.dart';
import 'redux/states.dart';

enum _PlayerState { stopped, playing, paused, loading }

class Player extends StatefulWidget {

  Player();

  @override
  State<StatefulWidget> createState() => new PlayerPanelState();
}

class PlayerPanelState extends State {

  @override
  Widget build(BuildContext context) {
    SongDatabase database = SongDatabase.getInstance();

    return new Container(
      // padding: const EdgeInsets.all(8.0),
      decoration: new BoxDecoration(
        color: Colors.white,
        boxShadow: [new BoxShadow(
          color: Colors.black,
          blurRadius: 8.0,
          offset: const Offset(0.0, 5.0)
        )]
      ),
      child: new StoreConnector<CancioneroPlayerState, CancioneroPlayerState>(
        converter: (store) => store.state,
        builder: (context, state) {
          if (state.currentSong.id == -1)
            return new Container();
          return new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Padding(
                          padding: const EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 0.0),
                          child: new Text(
                            state.currentSong.name,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.bodyMedium.copyWith(fontSize: 16.0, fontWeight: FontWeight.w600)
                          ),
                        ),
                        new SliderTheme(
                            data: SliderTheme.of(context).copyWith(
                              thumbColor: Colors.orange,
                              valueIndicatorColor: Colors.orange,
                              inactiveTickMarkColor: Colors.orange.shade100,
                              inactiveTrackColor: Colors.orange.shade100,
                              activeTickMarkColor: Colors.orange,
                              activeTrackColor: Colors.orange
                            ),
                            child: new Slider(
                              min: 0.0,
                              max: max(state.songDuration.inSeconds.toDouble(), state.songPosition.inSeconds.toDouble()),
                              value: state.songPosition.inSeconds.toDouble(),
                              onChanged: (position)  => StoreProvider.of<CancioneroPlayerState>(context).dispatch(new SeekAction(position.toInt()))
                            ),
                          ) 
                        
                      ],
                    ),
                  ),
                  new Container(
                    child: _getPlayerControl(),
                    width: 48.0,
                    height: 48.0,
                  ),
                  // new Container(
                  //   height: 48.0,
                  //   child: new Image.asset('assets/una_decision_300x300.png'),
                  // ),
                ]
              ),
            ]
          );
        },
      ) 
    );
  }

  Widget _getPlayerControl() {
    // if (state.playbackState == PlaybackState.loading) {
    //   return new Container(
    //       padding: const EdgeInsets.all(10.0),
    //       child: new CircularProgressIndicator(
    //       value: null,
    //     )
    //   );
    // } else {
      return new StoreConnector<CancioneroPlayerState, VoidCallback>(
        converter: (store) {
          return () => store.dispatch(store.state.playbackState == PlaybackState.Playing ? PlaybackAction.Pause : PlaybackAction.Play);
        },
        builder: (context, callback) {
          return new FlatButton(
            padding: EdgeInsets.zero,
            child: new StoreConnector<CancioneroPlayerState, PlaybackState>(
                converter: (store) => store.state.playbackState,
                builder: (context, playbackState) {
                  return new Icon(
                    playbackState == PlaybackState.Playing ? Icons.pause : Icons.play_arrow,
                    size: 48.0
                  );
                },
            ),
            onPressed: callback
          );
        },
      );
  }
}