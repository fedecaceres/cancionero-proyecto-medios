import 'package:redux_persist/redux_persist.dart';

import 'states.dart';
import 'actions.dart';

CancioneroPlayerState playerReducer(CancioneroPlayerState state, dynamic action) {
  if (action is SetPositionAction) {
    print("Position: " + action.position.toString());
    return state.copyWith(songPosition: action.position);
  } else if (action is SetDurationAction) {
    print("Duration: " + action.duration.toString());
    return state.copyWith(songDuration: action.duration);
  } else if (action is SelectSongAction) {
    return state.copyWith(
      currentSong: action.song,
      playbackState: PlaybackState.Stopped,
      songDuration: new Duration(),
      songPosition: new Duration()
    );
  } else if (action is PlaybackAction) {
    PlaybackState targetState;
    Duration targetPosition = state.songPosition;
    if (action == PlaybackAction.Toggle) {
      if (state.playbackState == PlaybackState.Playing) {
        targetState = PlaybackState.Paused;
      } else {
        targetState = PlaybackState.Playing;
      }
    } else if (action == PlaybackAction.Play) {
      targetState = PlaybackState.Playing;
    } else if (action == PlaybackAction.Pause) {
      targetState = PlaybackState.Paused;
    } else if (action == PlaybackAction.Complete) {
      targetState = PlaybackState.Stopped;
      targetPosition = new Duration();
    } else {
      targetState = PlaybackState.Stopped;
    }

    return state.copyWith(
      playbackState: targetState,
      songPosition: targetPosition
    );
  } else if (action is AddFavoriteAction) {
    List<int> newFavorites = List.from(state.favorites);
    newFavorites.add(action.songId);

    return state.copyWith(favorites: newFavorites);
  } else if (action is RemoveFavoriteAction) {
    List<int> newFavorites = List.from(state.favorites);
    newFavorites.remove(action.songId);

    return state.copyWith(favorites: newFavorites);
  // } else if (action is PersistLoadedAction<PlayerState>) {
  //   return action.state ?? state;
  }
  
  // noop
  return state;
}