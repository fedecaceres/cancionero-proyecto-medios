import '../database.dart';

class SelectSongAction {
  Song song;

  SelectSongAction(this.song);
}

enum PlaybackAction { Play, Pause, Stop, Toggle, Complete }

class SetDurationAction {
  Duration duration;

  SetDurationAction(this.duration);
}

class SetPositionAction {
  Duration position;

  SetPositionAction(this.position);
}

class SeekAction {
  int seconds;

  SeekAction(this.seconds);
}

class AddFavoriteAction {
  int songId;

  AddFavoriteAction(this.songId);
}

class RemoveFavoriteAction {
  int songId;

  RemoveFavoriteAction(this.songId);
}

// class PersistLoadedAction {

// }