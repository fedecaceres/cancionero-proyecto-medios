import '../database.dart';
import 'package:audioplayers/audioplayers.dart';

enum PlaybackState { Stopped, Playing, Paused, Loading }

class CancioneroPlayerState {
  final AudioPlayer audioPlayer;
  final Song currentSong;
  final PlaybackState playbackState;
  final Duration songDuration;
  final Duration songPosition;
  final List<int> favorites;

  const CancioneroPlayerState({this.audioPlayer, this.currentSong, this.playbackState, this.songDuration, this.songPosition, this.favorites});

  CancioneroPlayerState.initialState() :
    audioPlayer = new AudioPlayer(),
    currentSong = new Song(-1, 'No song', '', '', -1),
    playbackState = PlaybackState.Stopped,
    songDuration = new Duration(),
    songPosition = new Duration(),
    favorites = [];

  copyWith({audioPlayer, currentSong, playbackState, songDuration, songPosition, favorites}) {
    return new CancioneroPlayerState(
      audioPlayer: audioPlayer ?? this.audioPlayer,
      currentSong : currentSong ?? this.currentSong,
      playbackState : playbackState ?? this.playbackState,
      songDuration : songDuration ?? this.songDuration,
      songPosition : songPosition ?? this.songPosition,
      favorites : favorites ?? this.favorites
    );
  }

  static CancioneroPlayerState fromJson(dynamic json) =>
    CancioneroPlayerState
      .initialState()
      .copyWith(
        favorites: json['favorites'].cast<int>()
      );

  dynamic toJson() => {'favorites' : this.favorites};
}

