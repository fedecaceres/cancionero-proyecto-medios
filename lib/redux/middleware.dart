import 'package:redux/redux.dart';
import 'package:redux_persist/redux_persist.dart';

import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audioplayers_api.dart';


import 'actions.dart';
import 'states.dart';

/**
 * The middleware talks to the audioplayer before...
 * the reducer updates the store's state
 */
playbackMiddleware(
    Store<CancioneroPlayerState> store, dynamic action, NextDispatcher next) async {
  print('${new DateTime.now()}: $action');

  if (action is SelectSongAction) {
    store.state.audioPlayer.stop(); // not sure about this one
  } else if (action is SeekAction) {
    await store.state.audioPlayer.seek(Duration(seconds: action.seconds));
  } else if (action is PlaybackAction) {
    if (action == PlaybackAction.Play) {

      // Logger.changeLogLevel(LogLevel.INFO);
      // AudioPlayer audioPlayer = AudioPlayer();
      // audioPlayer.play("https://datashat.net/music_for_programming_63-t-flx.mp3");
      // audioPlayer.setVolume(1.0);

      print("================================");
      print(store.state.currentSong.downloadUrl);
      await store.state.audioPlayer.play(store.state.currentSong.downloadUrl);
    } else if (action == PlaybackAction.Pause) {
      await store.state.audioPlayer.pause();
    } else {
      await store.state.audioPlayer.stop();
    }
  }

  next(action);
}
