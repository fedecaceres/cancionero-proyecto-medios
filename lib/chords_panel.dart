import 'package:flutter/material.dart';

class ChordPanel extends StatelessWidget {

  final List<String> chords;

  ChordPanel(this.chords);

  String _sanitizeChordName(String chordName) {
    print(chordName);
    return chordName.replaceAll('/', '_').replaceAll('#', 's');
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: 100.0,
      // width: double.INFINITY,
      decoration: new BoxDecoration(
        color: Colors.white,
        boxShadow: [new BoxShadow(
          color: Colors.black,
          blurRadius: 10.0,
          offset: const Offset(0.0, 5.0)
        )]
      ),
      child: new ListView(
        padding: const EdgeInsets.all(8.0),
        scrollDirection: Axis.horizontal,
        children: this.chords.map((chord) {
          return new Image.asset('assets/chords/' + this._sanitizeChordName(chord) + '.png');
        }).toList(),)
    );
  }
}