import 'dart:async' show Future;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_redux/flutter_redux.dart';

import 'database.dart';
import 'player.dart';
import 'chords_panel.dart';
import 'lyrics.dart';
import 'redux/actions.dart';
import 'redux/states.dart';

class SongView extends StatefulWidget {

  Song song;

  SongView(this.song);

  @override
  State<StatefulWidget> createState() => new SongState(song);
}

class _PlayFABViewModel {
  bool isSameSong;
  VoidCallback callback;

  _PlayFABViewModel({this.isSameSong, this.callback});
}

class SongState extends State {

  Song song;
  bool _showChords = true;

  SongState(this.song);

  @override
  Widget build(BuildContext context) {
    Scaffold s = new Scaffold(
      appBar: new AppBar(
        // title: new Text(this.song.name),
        backgroundColor: Colors.transparent,
        textTheme: Theme.of(context).textTheme.apply(bodyColor: Colors.orange),
        iconTheme: Theme.of(context).iconTheme.copyWith(color: Colors.orange),
        elevation: 0.0,
        // textTheme: Theme.of(context).textTheme,
        // iconTheme: Theme.of(context).iconTheme,
        actions: <Widget>[
          new StoreConnector<CancioneroPlayerState, bool>(
            converter: (store) => store.state.favorites.contains(this.song.id),
            builder: (context, isFavorite) {
              return new IconButton(
                icon: new Icon(isFavorite ? Icons.favorite : Icons.favorite_border),
                color: isFavorite ? Colors.orange : Colors.grey.shade400,
                highlightColor: Colors.teal.withAlpha(50),
                onPressed: () {
                  StoreProvider.of<CancioneroPlayerState>(context).dispatch(
                    isFavorite ?
                    new RemoveFavoriteAction(this.song.id) :
                    new AddFavoriteAction(this.song.id)
                  );
                },
              );
            }
          ),
          new IconButton(
            icon: new Icon(_showChords ? Icons.grid_on : Icons.grid_off),
            color: _showChords ? Colors.orange : Theme.of(context).disabledColor,
            onPressed: this._toggleChords,
          ),
          new PopupMenuButton(
            itemBuilder: (BuildContext context) {
              return [
                new PopupMenuItem(
                  value: 'share',
                  child: new ListTile(
                    leading: const Icon(Icons.share),
                    title: const Text('Compartir...')
                  )
                )
              ];
            },
          )
        ],
      ),
      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 10.0),
            child: new Text(
              song.name,
              style: Theme.of(context).textTheme.titleMedium.apply(color: Colors.orange).copyWith(fontWeight: FontWeight.w400)
            ),
          ),
          new Expanded(
            child: new Stack(
              alignment: Alignment.bottomRight,
              children: <Widget>[
                new SingleChildScrollView(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  child: new Lyrics(song.rawLyrics ?? '', showChords: this._showChords,),
                ),
                new Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: new StoreConnector<CancioneroPlayerState, _PlayFABViewModel>(
                    converter: (store) => new _PlayFABViewModel(
                      isSameSong: store.state.currentSong.id == song.id,
                      callback: () {
                        store.dispatch(new SelectSongAction(song));
                        store.dispatch(PlaybackAction.Play);
                      }
                    ),
                    builder: (context, playFABViewModel) {
                      if (!playFABViewModel.isSameSong) {
                        return new FloatingActionButton(
                          child: new Icon(Icons.play_arrow),
                          onPressed: playFABViewModel.callback,
                        );
                      }
                      return new Container();
                    },
                  ),
                ),
              ],
            ),
          ),
          this._showChords ? new ChordPanel(song.getChords()) : new Container(),
          new Player(),
        ],
      )
    );
    return s;
  }

  _toggleChords() {
    setState(() {
      this._showChords = !this._showChords;
    });
  }
}

Future<String> loadSongText() async {
  return await rootBundle.loadString('assets/songs/test_song.chord');
}