import 'dart:ui';

class AppColors {
  static const primary = Color(0xFFFDA160);
  static const dark = Color(0xFFFE6E36);
  static const accent = Color(0xFF43D5A4);
  static const background = Color(0xFFFFFFFF);
  static const text = Color(0xFFA3ADBE);
}