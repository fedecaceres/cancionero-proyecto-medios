import 'package:flutter/material.dart';

import 'routes.dart';
import 'player.dart';
import 'database.dart';
import 'song.dart';
import 'theme.dart';
import 'ui.dart';

class SongListView extends StatefulWidget {
  
  int albumId;

  SongListView(this.albumId);
  
  @override
  createState() => new SongListState(this.albumId);
}

class SongListState extends State {

  int albumId;

  SongListState(this.albumId);
  
  @override
  Widget build(BuildContext context) {

    SongDatabase _db = SongDatabase.getInstance();
    Album album = _db.getAlbum(this.albumId);
    List<Widget> _presetWidgets = [
      new Container(
        child: new Padding(
          padding: EdgeInsets.symmetric(horizontal: 13.0),
          child: new Material(
            color: Colors.transparent,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            child: Hero(
              tag: "album-cover-${this.albumId}",
              child: Container(
                clipBehavior: Clip.antiAliasWithSaveLayer,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Image.asset(this._getAlbumCoverAssetString(albumId), fit: BoxFit.fill),
                // child: Text("asd"),
              )
            )
          ),
        )
      ),
      new Container(
        child: new ListTile(
          title: new Text("Descargar", style: Theme.of(context).textTheme.bodyMedium.copyWith(fontWeight: FontWeight.bold, fontSize: 16.0)),
          trailing: new Switch(
            value: true,
            onChanged: (bool newValue) {},
            activeColor: CancioneroColors.textOnPrimaryColor,
          ),
        ),
      )
      
    ];

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          album.name,
          style: Theme.of(context).textTheme.titleLarge.apply(color: Colors.white).copyWith(fontWeight: FontWeight.w400),
        ),
        textTheme: Theme.of(context).primaryTextTheme.apply(bodyColor: Colors.white),
        iconTheme: Theme.of(context).primaryIconTheme.copyWith(color: Colors.white),
        backgroundColor: Colors.orange,
        elevation: 0.0,
      ),
      backgroundColor: Colors.orange,
      body : new Column(
        children: <Widget>[
          new Expanded(
            child: new ListView.builder(
              itemCount: album.songs.length + 2,
              itemBuilder: (context, i) {
                if (i >= 0 && i <= 1) {
                  return _presetWidgets[i];
                }
                List<Song> songs = album.songs;
                if (i < songs.length + 2) return Ink(color: Colors.white, child: _buildRow(songs[i-2]));
              }
            ),
          ),
          new Player()
        ],
      )
    );
  }

  Widget _buildRow(Song song) {
    return new SongTile(
      song: song,
      onTap: ()  => _goToSong(song)
    );
  }

  _goToSong(Song song) {
    Navigator.of(context).push(
      new CustomRoute(
        builder: (context) {return new SongView(song);}
      )
    );
  }

  _getAlbumCoverAssetString(int albumId) {
    List<String> coverAssetStrings = [
      'assets/una_decision_300x300.png',
      'assets/sembrando_futuro_300x300.png',
      'assets/avanzo_con_valor_300x300.jpg',
      'assets/claridad_300x300.png'
    ];

    return coverAssetStrings[albumId - 1];
  }

}